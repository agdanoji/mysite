django-poll-app

\\Getting Started

Make a new virtualenv: virtualenv env
Activate the virtualenv: source env/bin/activate
Install Django: pip install Django
Edit mysite/settings.py:36 to match your timezone
Run the server: python manage.py runserver
Open website in browser at http://localhost:8000/polls or admin at http://localhost:8000/admin (admin:admin)

\\After initial setup

Activate the virtualenv: source env/bin/activate
Run the server: python manage.py runserver
Open website in browser at http://localhost:8000/polls or admin at http://localhost:8000/admin (admin:admin)

\\Installation

pip install mysite

Install latest from github:

pip install -e git+https://gitlab.com/agdanoji/mysite